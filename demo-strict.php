<?php
	$content = 'Demo <b>content</b>!';

	if (isset($_POST['override'])) {
		$_POST['html'] = $_POST['override'];
	}

	if (isset($_POST['html'])) {
		$submitted = $_POST['html'];
		// note: this allows certain tags, but not any attributes or whitespace within them
		$allowedTags = explode(' ', 'br strong b i ol ul li div p');
		$tmp = $submitted;
		foreach ($allowedTags as $tag) {
			$tmp = str_replace("<$tag>", '', $tmp);
			$tmp = str_replace("</$tag>", '', $tmp);
		}
		if (strpos($tmp, '<') !== false) {
			print('Unsafe content detected. Submission rejected.');
		}
		else {
			print('Submitted: <pre>');
			print(htmlentities($submitted));
			print('</pre>');
			$content = $submitted;
		}
	}
?>
<form method=POST>
	<div class=whatyougetsy data-form-field-name=html><?php echo $content; ?></div>
	Or override the above field:<br>
	<textarea name=override cols=80 rows=8></textarea><br>
	<input type=submit>
</form>
<script src='whatyougetsy.js'></script>
