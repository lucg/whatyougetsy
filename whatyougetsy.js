(function() {  // scope our shenanigans to avoid polluting the outer/global scope
	this.applyDefaultStyles = (document.currentScript.dataset.defaultStyles !== 'no');
	this.ignoreMouseUp = false;
	this.ignoreBlurEvent = false;

	this.tools = [
		{
			buttonContent: '<b style="font-family: monospace">B</b>',
			apply: function(ev) {
				// execCommand is deprecated, but the alternative is rather unwieldy and this basic command seems to universally work. Replace as/if/when necessary...
				document.execCommand('bold', false);
			},
			tagFilter: 'strong, b',
		},
		{
			buttonContent: '<i style="font-family: monospace">I</i>',
			apply: function(ev) {
				document.execCommand('italic', false);
			},
			tagFilter: 'i',
		},
		{
			buttonContent: '&bullet;',
			apply: function(ev) {
				document.execCommand('insertUnorderedList', false);
			},
			tagFilter: 'ul, li',
		},
		{
			buttonContent: '1.',
			apply: function(ev) {
				document.execCommand('insertOrderedList', false);
			},
			tagFilter: 'ol, li',
		},
	];

	this.toolbar = document.createElement('div');
	toolbar.className = 'whatyougetsyToolbar';
	toolbar.style.position = 'absolute';
	if (this.applyDefaultStyles) {
		toolbar.style.backgroundColor = 'white';
		toolbar.style.border = '1px solid #888';
		toolbar.style.borderRadius = '4px';
	}
	toolbar.hide = function() {
		toolbar.style.display = 'none';
	};
	toolbar.show = function() {
		toolbar.style.display = 'block';
	};
	toolbar.isShown = function() {
		return toolbar.style.display == 'block';
	}

	// add any allowed tags here that you can't create with the toolbar. The `tagFilter` values from the toolbar will be automatically added
	this.allowedTagFilters = 'br';

	for (let tool in tools) {
		let btn = document.createElement('button');
		btn.className = 'whatyougetsyToolbarButton';
		btn.innerHTML = tools[tool].buttonContent;
		if (toolbar.children.length > 0 && this.applyDefaultStyles) {
			btn.style.display = 'inline-block';
			btn.style.marginLeft = '5px';
		}
		let mousedown = function(ev) {
			// don't handle the click here because people might move off of the button still
			ignoreBlurEvent = true;
		};
		btn.addEventListener('mousedown', mousedown);
		btn.addEventListener('touchstart', mousedown);
		btn.addEventListener('click', function(ev) {
			tools[tool].apply(ev);
		});
		toolbar.append(btn);

		allowedTagFilters += ',' + tools[tool].tagFilter;
	}

	this.cleanDOMTree = function(tree, allowedTagFilters, verbose) {
		// HTML elements that can have contents (i.e., are not void tags), but whose contents are not meant to be shown to the user in ordinary cases.
		// E.g. LibreOffice includes <style>text</style> when copying things. Perhaps other sources include more of these tags.
		// As a counterexample: <form> does not render, but its contents can be shown and so it should *not* be in this list.
		const hiddenContentsTags = ['style', 'noscript', 'script', 'title', 'iframe', 'datalist', 'template', 'canvas', 'audio', 'video'];

		// remove tags before removing attributes so that we don't have to bother checking for attributes on removed tags
		tree.querySelectorAll(`:not(${allowedTagFilters})`).forEach((badTag) => {
			// move the child nodes to the parent element by replacing this element with its children
			if (hiddenContentsTags.indexOf(badTag.tagName.toLowerCase()) > -1) {
				if (verbose) {
					console.log(`Whatyougetsy editor: removing disallowed non-content tag "${badTag.tagName}"`);
				}
				badTag.parentNode.removeChild(badTag);
			}
			else {
				if (badTag.tagName.toLowerCase() == 'p') {
					// special case. Instead of <p>, we use <br>, because the contenteditable seems not to use it except when there's
					// already one present which is just confusing (sometimes line breaks are longer?!) and normal line breaks just seem a lot simpler.
					//badTag.parentNode.insertBefore(document.createElement('br'), badTag);  TODO do we need one before?
					badTag.appendChild(document.createElement('br'));
					// Typically, <p> represents something like 1.5 <br>s, and in ascii markup (markdown, plain text) it is customary to
					// use a blank line for paragraph demarcation, but from testing, one <br> clearly gives a more expected result.
				}
				else if (verbose) {
					console.log(`Whatyougetsy editor: removing disallowed tag "${badTag.tagName}", moving contents to the parent element`);
				}
				badTag.replaceWith(...badTag.childNodes);
			}
		});

		tree.querySelectorAll('*').forEach((el) => {
			// we only want base tags like <i> and <li>, no potentially unsafe attributes like onhover=script
			for (let attr of el.attributes) {
				if (verbose) {
					console.log(`Whatyougetsy editor: removing attribute "${attr.name}" from element`, el);
				}
				el.removeAttribute(attr.name);
			}
		});
	}

	document.querySelectorAll('.whatyougetsy').forEach((editorElement) => {
		let submissionElement = null;
		if ('formFieldName' in editorElement.dataset) {
			// if the website owner set a form field name, i.e. wants to use form submission, then create a hidden textarea which we can put the contents into
			submissionElement = document.createElement('textarea');
			submissionElement.style.display = 'none';
			submissionElement.name = editorElement.dataset.formFieldName;
			editorElement.parentNode.insertBefore(submissionElement, editorElement.nextSibling);

			// find the <form> element which we're in and bind updating the hidden textarea to onsubmit
			let elementPtr = editorElement.parentNode;
			let maxLoops = 5000;  // prevent an infinite loop
			while (elementPtr != null && maxLoops-- > 0) {
				if (elementPtr.tagName.toLowerCase() == 'form') {
					elementPtr.addEventListener('submit', function(ev) {
						editorElement.updateFormField();
					});
					break;
				}
				elementPtr = editorElement.parentNode;
			}
		}

		// Clean the current, pre-filled content to prevent surprises later.
		// Run it verbose because the loaded-from-server content shouldn't typically contain disallowed elements
		this.cleanDOMTree(editorElement, allowedTagFilters, true);

		editorElement.contentEditable = true;

		// If the parent element is bold, our editor will inherent that default font weight and then
		// apparently execCommand(bold) will change its behavior and start using <p style='font-weight: normal'> for unbolded parts instead of <b> for bolded parts...
		// Unset any such default. Because it's breakage-prevention rather than a style preference (a text input should not be bold by default unless you explicitly set it),
		// this is outside of the applyDefaultStyles condition. It can always still be overridden with `!important`.
		editorElement.style.fontWeight = 'normal';

		if (this.applyDefaultStyles) {
			editorElement.style.padding = '5px';
			editorElement.style.overflow = 'auto';
			editorElement.style.resize = 'both';

			editorElement.style.width = '500px';
			editorElement.style.height = '200px';
			editorElement.style.border = '1px solid #334';
		}

		editorElement.addEventListener('paste', function(ev) {
			// allow the standard browser functionality to run, then clean up
			setTimeout(function() {
				this.cleanDOMTree(editorElement, allowedTagFilters);
			}, 1);
		});

		let mouseup = function(ev) {  // anonymous function syntax to have 'let' scoping rather than global scoping
			if (ignoreMouseUp && document.getSelection().toString().length == 0) {
				ignoreMouseUp = false;
				return;
			}
			ignoreMouseUp = false;
			toolbar.style.left = (ev.clientX + window.scrollX + 1) + 'px';
			toolbar.style.top = (ev.clientY + window.scrollY+ 16) + 'px';
			toolbar.show();
		}
		editorElement.addEventListener('mouseup', mouseup);
		editorElement.addEventListener('selectionchange', mouseup);

		let mousedown = function(ev) {
			if (!ev.touches && ev.button !== 0) {
				// not the left mouse button, and not a touch event
				ignoreMouseUp = true;
				return;
			}
			if (document.activeElement != editorElement) {
				// we don't have focus; ignore this focusing click
				ignoreMouseUp = true;
				return;
			}

			if (toolbar.isShown()) {
				// if it's already open and the user clicks next to it, they want it to be gone
				toolbar.hide();
				ignoreMouseUp = true;
				return;
			}
			else if (ev.touches) {
				// probably start of a selection, otherwise it would be a mouse event and not a touch event
				toolbar.style.left = (ev.touches[0].pageX + 1) + 'px';
				toolbar.style.top = (ev.touches[0].pageY + 48) + 'px';
				toolbar.show();
			}
		}
		editorElement.addEventListener('mousedown', mousedown);
		editorElement.addEventListener('touchstart', mousedown);

		editorElement.addEventListener('blur', function(ev) {
			if (ignoreBlurEvent) {
				ignoreBlurEvent = false;
				return;
			}
			toolbar.hide();
		});

		editorElement.addEventListener('keydown', function(ev) {
			// if you start typing, clearly you didn't want the toolbar there
			toolbar.hide();
		});

		editorElement.updateFormField = function() {
			if (submissionElement !== null) {
				cleanDOMTree(editorElement, allowedTagFilters, true);  // run verbosely because the user shouldn't have been able to introduce disallowed elements
				submissionElement.value = editorElement.innerHTML.trim();
				return true;
			}
			return false;
		};
	});

	toolbar.hide();
	document.body.appendChild(toolbar);

	window.whatyougetsy = this;  // allow global scope to access the variables defined here
})();  // immediately run this anonymous function

