import html
import flask

webserver = flask.Flask(__name__)

allowed_tags = 'br strong b i ol ul li div p'.split(' ')

def isSafeHtml(html):
    tmp = str(html)
    for tag in allowed_tags:
        tmp = tmp.replace(f'<{tag}>', '').replace(f'</{tag}>', '')

    return '<' not in tmp


@webserver.route('/', methods=['GET', 'POST'])
def main():
    content = 'Demo <b>content</b>!'
    response = ''

    if 'html' in flask.request.form:
        submitted = flask.request.form['html']
        if isSafeHtml(submitted):
            response += 'Submitted: <pre>' + html.escape(submitted) + '</pre>'
            content = submitted
        else:
            response += 'Unsafe content detected. Submission rejected.<br>'

    response += f'''
        <form method=POST>
            <div class=whatyougetsy data-form-field-name=html>{content}</div>
            <input type=submit>
        </form>
        <script src='whatyougetsy.js'></script>
    '''

    return flask.Response(response)


@webserver.route('/whatyougetsy.js')
def whatyougetsyjs():
    return flask.send_file('whatyougetsy.js')


if __name__ == '__main__':
    webserver.run(port=3000)

